## Required imports
from flask import Flask, url_for, redirect
import os
import uuid

from models.models import *
from db.mongo import MongoConnector # Required to access db in this way
from optparse import OptionParser
from mongoengine.errors import ValidationError


ONNX_FILES_FOLDER = "./tmpONNX/"
SESAME_FILES_FOLDER = "./tmpSESAME/"

def recursivelyDeleteFolder(path):
    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))




if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-c", "--cleanDB", dest="cleanDB",
                      help="Clean Data base", metavar="", default=0)


    (options, args) = parser.parse_args()

    option_dict = vars(options)
    cleanDB = bool(option_dict['cleanDB'])

"""


1. CREATING FLASK APPLICATION + CONNECTING TO MongoDB


"""
## Creating Flask application to reproduce modules set up and environment.
app = Flask(__name__)

###
#  Configuring MongoDB access
###
app.config['MONGODB_SETTINGS'] = {
    'db': 'aloha-test', #Name of the database
    'host': "localhost", # Host of the DB
    'port': 27017, # DB port
    #'username':'aloha',
    #'password':'pwd123'
}

## Start a new connection with the database (Required to use MongoEngine).
mongo = MongoConnector(app)

if cleanDB: # Removing content from DB by default
    mongo.db.drop_collection('user')
    mongo.db.drop_collection('dataset')
    mongo.db.drop_collection('project')
    mongo.db.drop_collection('project_constrains')
    mongo.db.drop_collection('configuration_file')
    mongo.db.drop_collection('algorithm_configuration')
    mongo.db.drop_collection('architecture')

    recursivelyDeleteFolder(ONNX_FILES_FOLDER)
    recursivelyDeleteFolder(SESAME_FILES_FOLDER)


    print ("- Database cleaned.")
else:
    print ("- Not cleaning DB. Use [--cleanDB 1] to activate this behaviour.")

"""


2. COUNTING OBJECTS ON DB WHEN STARTING EXECUTION


"""
init_users_count = User.objects.count() #Getting starting number of users from DB
init_dataset_count = Dataset.objects.count()
init_projects_count = Project.objects.count()
init_constrains_count = ProjectContrains.objects.count()
init_config_files_count = ConfigurationFile.objects.count()
init_algorithm_conf_count = AlgorithmConfiguration.objects.count()
init_architecture_count = Architecture.objects.count()


"""


3. CREATING OBJECTS ON DB. NOTE SOME OF THEM REQUIRE PARENT OBJECTS TO BE CREATED FIRST (as project requires an user to be created first).


"""

"""
## IMPORTANT!
## when querying DB:
### Using ([ClassName].objects.get) will look for one object. Using a query that returns more than one object will fire a MultipleObjectsReturned exception.
### To objtain multiple objets, ([ClassName].objects) is equivalent to ([ClassName].objects.filter)
"""

## Creating some objects and persist them in DB
user = User()
user.email = "fake@email.com"
user.password = "hidden_pass"
user.save() # Store it in DB

assert User.objects.count() == init_users_count+1 #Testing new user was added to DB

## Training dataset
data = Dataset()
data.path = "Fake/Path/To/Dataset"
data.samples_id = ["Fake ID1","Fake ID2","Fake ID3","Fake ID4"]
data.labels = ["Positive", "Negative", "Positive", "Negative"]
data.data_resolution = [80,80]
data.encryption_type = "Encryption_type"
data.label_x = [15,25,35,45]
data.label_y = [55,65,75,80]
data.training_flag = [True,False,False,False]
data.save()

assert Dataset.objects.count()==init_dataset_count+1 #Testing dataset was added to DB


# Creating Project.
proj = Project()
proj.creator = user.get_id()
proj.dataset=data.get_id()

proj.save()

assert Project.objects.count()==init_projects_count+1 #Testing new project was added to DB

##Creating contrains
proj_const = ProjectContrains()
proj_const.power_value = 1
proj_const.power_priority = 2

proj_const.security_value = 3
proj_const.security_priority = 4

proj_const.execution_time_value = 5
proj_const.execution_time_priority = 6

proj_const.accuracy_value = 7
proj_const.accuracy_priority = 8


proj_const.project = proj.get_id()
proj_const.save()

assert ProjectContrains.objects.count()==init_constrains_count+1 #Testing new project was added to DB

## Configuration file
conf_file = ConfigurationFile()
conf_file.project = proj.get_id()
conf_file.path = "Fake/Path/To/File"
conf_file.save()

assert ConfigurationFile.objects.count()==init_config_files_count+1 #Testing configuration files was added to DB

## AlgorithmConfiguration file
alg_file = AlgorithmConfiguration()
alg_file.project = proj.get_id()
alg_file.path = "Fake/Path/To/File"
alg_file.save()

assert AlgorithmConfiguration.objects.count()==init_algorithm_conf_count+1 #Testing algorithm configuration files was added to DB


## Architecture description file
arch = Architecture()
arch.path = "Fake/Path/To/Architecture/Description"
arch.project = proj.get_id()
arch.save()

assert Architecture.objects.count()==init_architecture_count+1


## Retrieve objects by ID.
user2 = User.objects.get(id=user.get_id())
assert  user2.email == user.email

data2 = Dataset.objects.get(id=data.get_id())
assert  data2.path == data.path
assert  data2.labels == data.labels
assert  data2.encryption_type == data.encryption_type

proj2 = Project.objects.get(id=proj.get_id())
assert proj2.creator == proj.creator
assert proj2.dataset == proj.dataset


## Trying to retrieve object from a unvalid id raises an error
try:
    user3 = User.objects.get(id="not_valid_id") ##Looking for an ID which is not an ObjectID
    assert False #Should never reach this point
except ValidationError:
    assert True


try:
    user3 = User.objects.get(id=proj.get_id())
    assert False #Should never reach this point
except User.DoesNotExist:
    assert True



#You can retrieve objects by any other property:
user4 = User.objects.get(email=user.email)
assert  user4.email == user.email



"""


4. WRITTING FILES ON FILE SYSTEM + STORING METADATA IN DB


"""

def createFolderIfNotExists(folder_path):

    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

def generate_filename(path, extension):
    xx = str(uuid.uuid4()) + extension
    if os.path.isfile(os.path.join(path, xx)):
        return generate_filename(extension)
    return xx

def writeFile(path, content, extension):

    file_name = generate_filename(path, extension)
    file_path = os.path.join(path, file_name)
    with open(file_path, 'a') as f:
        f.write(content)

    return os.path.join(path, file_path)




## Write ONNX file in file system + add metadata on DB.
def writeONNXFile(folder_path, content, file_extension, parentProject):
    createFolderIfNotExists(ONNX_FILES_FOLDER)
    file_path = writeFile(folder_path, content, file_extension)

    alg_file = AlgorithmConfiguration()
    alg_file.project = parentProject.get_id()
    alg_file.path = file_path
    alg_file.save()

writeONNXFile(ONNX_FILES_FOLDER, "Test content", ".onnx", proj)


## Write SESAME file in file system + add metadata on DB.
def writeSESAMEFile(folder_path, content, file_extension, parentProject):
    createFolderIfNotExists(SESAME_FILES_FOLDER)

    file_path = writeFile(folder_path, content, file_extension)
    arch = Architecture()
    arch.path = file_path
    arch.project = parentProject.get_id()
    arch.save()

writeSESAMEFile(SESAME_FILES_FOLDER, "Test content", ".sesame",proj)


"""


5. RETRIEVING ALL INFORMATION RELATED TO A CERTAIN PROJECT OBJECT


"""

## Read all metadata related to a certain project ID. Includes the dataset used in the project
def getAllInfoFromProject(project):


    init_constrains_tmp = ProjectContrains.objects.filter(project=project.get_id())
    init_config_files_tmp = ConfigurationFile.objects.filter(project=project.get_id())
    init_algorithm_conf_tmp = AlgorithmConfiguration.objects.filter(project=project.get_id())
    init_architecture_tmp = Architecture.objects.filter(project=project.get_id())

    dataset_tmp = Dataset.objects.get(id=project.dataset)

    return init_constrains_tmp, init_config_files_tmp, init_algorithm_conf_tmp, init_architecture_tmp, dataset_tmp


## Retrieving information from a projectID
def getAllInfoFromProjectID(projectID):

    #By using Project.object
    project = Project.objects.get(id=projectID)
    init_constrains_tmp = ProjectContrains.objects.filter(project=projectID)
    init_config_files_tmp = ConfigurationFile.objects.filter(project=projectID)
    init_algorithm_conf_tmp = AlgorithmConfiguration.objects.filter(project=projectID)
    init_architecture_tmp = Architecture.objects.filter(project=projectID)

    dataset_tmp = Dataset.objects.get(id=project.dataset)



    return init_constrains_tmp, init_config_files_tmp, init_algorithm_conf_tmp, init_architecture_tmp, dataset_tmp


tmp_contrains, tmp_conf, tmp_alg_conf, tmp_arch, tmp_data = getAllInfoFromProject(proj)


assert len(tmp_contrains) == 1
assert len(tmp_conf) == 1
assert len(tmp_alg_conf) == 2
assert len(tmp_arch) == 2

assert tmp_contrains[0].project == proj.get_id()
assert tmp_conf[0].project == proj.get_id()
assert tmp_alg_conf[0].project == proj.get_id()
assert tmp_arch[0].project == proj.get_id()
assert tmp_data.path == data.path



tmp_contrains2, tmp_conf2, tmp_alg_conf2, tmp_arch2, tmp_data2 = getAllInfoFromProjectID(proj.get_id())
assert len(tmp_contrains2) == 1
assert len(tmp_conf2) == 1
assert len(tmp_alg_conf2) == 2
assert len(tmp_arch2) == 2

assert tmp_contrains2[0].project == proj.get_id()
assert tmp_conf2[0].project == proj.get_id()
assert tmp_alg_conf2[0].project == proj.get_id()
assert tmp_arch2[0].project == proj.get_id()
assert tmp_data2.path == data.path



print("-- Execution finished. All functions sucessfully executed")