.. darts documentation master file, created by
   sphinx-quickstart on Tue Dec 11 17:54:30 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to darts's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 4

   introduction
   usage
   ACSDFModel
   ActorModel
   Allocation
   CProcessor
   CSDFParser
   ChannelModel
   DotHandler
   EspamHandler
   PlatformGenerator
   PlatformParameters
   Processor
   SchedulabilityAnalysis
   Schedulers
   TextHandler
   Utilities

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

