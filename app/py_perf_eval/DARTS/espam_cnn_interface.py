import darts.darts.CSDFParser as parser
import os
import sys
import getopt

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Evaluates CSDF graph in terms of power/performance')
    parser.add_argument('c', metavar='c', type=str, action='store', help='command')
    parser.add_argument('d', metavar='d', type=str, action='store', help='path to source SDF models directory')
    parser.add_argument('f', metavar='f', type=str, action='store', help='source SDF model file')
    args = parser.parse_args()
    try:
        command = args.c
        command_recognized = False
        # load SDF graph
        graph = load_graph(args.d, args.f)
        if command == "eval":
            eval_graph(graph)
            command_recognized = True
        if command == "rep_vec":
            calc_rep_vec(graph)
            command_recognized = True

        if (command_recognized == False):
            raise Exception("unrecognized command")

   # except FileNotFoundError:
    #    print("Error: SDF model file not found")
        # in case of any DARTS internal errors
    except Exception:
        print("Error: Darts internal exception ")


def print_error(msg):
    print("Error: "+ msg)

""" Loading .json (analogue for .gph is ACSDFModel.load_from_file"""
# parameters:
# dir - file source directory
# filename - name of file
# scaling factor - by default = 1
# deadline_factor - the deadline factor of the graph. Must be real-valued
# and between [0,1]. by default deadline factor = 1.0
# boolean verbose - if there is a need to print details


def load_graph(dir,filename):
    file = get_file(dir,filename)
    if file is None:
        raise Exception

    #print("Graph file loaded")
    """ Parsing .json file to obtain ACSDF model"""
    #create parser instance
    splittedfilename = filename.split(".")
    model_name = splittedfilename[0]

    # create JSON parser instance
    parserInstance = parser.CSDFParser(model_name, file)

    # parse graph from loaded file
    graph = parser.CSDFParser.parse_graph(parserInstance)
    return graph

def eval_graph(graph, scaling_factor=1, deadline_factor=1.0, verbose=False, printRes=True):
    """Calculate and set loaded graph parameters for
     parsed graph (analogue for .gph is ACSDFModel.load_from_file)"""
    graph.set_deadline_factor(deadline_factor)
    graph.compute_repetition_vector()
    if verbose:
        sys.stdout.write("Computing the minimum period vector...\n")
    graph.find_minimum_repetition_vector()

    graph.set_scaling_factor(scaling_factor)
    graph.find_minimum_period_vector(graph.get_scaling_factor())

    for a in graph.get_actors():    # Initial value of deadline
        a.set_deadline(a.get_period())
    # Updates the start time and deadline
    graph.find_earliest_start_time_vector()

    """Calculate start time buffer vector """
    graph.calc_start_time_buffer_vector()

    if(verbose):
        print_parameters(graph)

    """Total buffer size """
    total_buffer_size = 0
    for c in graph.get_channels():
        total_buffer_size += c.get_buffer_size()

    if(verbose):
        print_total_buffer_size(total_buffer_size)

    # if graph was sucessfully loaded, evaluate it and print results to output stream
    if(printRes):
        print_evaluation_results(graph)

    return graph

def calc_rep_vec(graph, deadline_factor=1.0, verbose=False, printRes=True):
    """Calculate and set loaded graph parameters for
     parsed graph (analogue for .gph is ACSDFModel.load_from_file)"""
    graph.set_deadline_factor(deadline_factor)
    if verbose:
        sys.stdout.write("Computing the repetition vector...\n")
    graph.compute_repetition_vector()
    graph.find_minimum_repetition_vector()

    # if graph was sucessfully loaded, evaluate it and print results to output stream
    if(printRes):
        sys.stdout.write(get_repetition_vector_str_with_ids(graph))

    return graph

"""Writes parameters, obtained afther SDFG evaluation to specified file"""
# parameters:
# dir - SDF model file source directory
# filename - name of file with SDF model
# scaling factor - by default = 1
# deadline_factor - the deadline factor of the graph. Must be real-valued
# and between [0,1]. by default deadline factor = 1.0
# boolean verbose - if there is a need to print details
def write_evaluation_results(graph, dir):
    #print("evaluation results writing...")
    #result_file_path = dir + os.sep + graph.name+"_result.json"
    result_file_path = dir
    mode = 'w' if os.path.exists(result_file_path) else 'a'
    with open(result_file_path, mode) as file:
        file.write(' {\n')
        file.write(' "performance": ' + str(graph.get_latency()) + ' ,\n')
        file.write(' "energy": ' + str(graph.get_utilization()) + ' ,\n')
        file.write(' "memory": ' + str(graph.get_total_buffer_size()))
        file.write('\n}\n')
    #print("evaluation results are written")


def get_repetition_vector_str_with_ids(graph):
    """Return the repetition vector as a string, using acto node ids"""
    repetition_vector = ["["]
    for i in graph.actors:
        repetition_vector.append("%s = %s, " % (i.get_actor_id(), i.get_repetition()))
    repetition_vector.append("]")
    return ''.join(repetition_vector)

"""Printss parameters, obtained afther SDFG evaluation to output stream"""
# parameters:
# dir - SDF model file source directory
# filename - name of file with SDF model
# scaling factor - by default = 1
# deadline_factor - the deadline factor of the graph. Must be real-valued
# and between [0,1]. by default deadline factor = 1.0
def print_evaluation_results(graph):
    print ('{\n')
    print (' "performance": ' + str(graph.get_latency()) + ' ,\n')
    print (' "energy": ' + str(graph.get_utilization()) + ' ,\n')
    print (' "memory": ' + str(graph.get_total_buffer_size()))
    print ("\n}\n")

def print_total_buffer_size (total_buffer_size):
    sys.stdout.write(" -- Total buffer size = %s\n" % (total_buffer_size))


def print_to_dot (graph, dir):
    """Print .dot representation of the graph into file """
    graph.print_graph_dot(graph,dir)


# get file from directory
def get_file(dir,name):
    fullpath = os.path.join(dir, name)
    file = None
    if os.path.isfile(fullpath):
        file = open(fullpath)

    return file

# read file
def read_file(fullpath):
    file = open(fullpath)
    for line in file:
        print(line)
    file.close()

# print whole bunch of the parametes calculated by DARTS
def print_parameters (graph):
    sys.stdout.write(" -- Number of actors = %s\n" % (graph.get_num_of_actors()))
    sys.stdout.write(" -- Repetition vector = %s\n" % (graph.get_repetition_vector_str()))
    sys.stdout.write(" -- Worst-case execution time vector = %s\n" % (graph.get_execution_vector_str()))
    sys.stdout.write(" -- Minimum period vector = %s\n" % (graph.get_period_vector_str()))
    sys.stdout.write(" -- Deadline vector = %s\n" % (graph.get_deadline_vector_str()))
    sys.stdout.write(" -- Start time vector = %s\n" % (graph.get_start_time_vector_str()))
    sys.stdout.write(" -- Buffer size vector = %s\n" % (graph.get_buffer_size_vector_str()))
    sys.stdout.write(" -- Total Utilization  = %s\n" % (graph.get_utilization()))
    sys.stdout.write(" -- Total Density = %s\n" % (graph.get_density()))
    sys.stdout.write(" -- Graph maximum latency = %s\n" % (graph.get_latency()))

if __name__ == "__main__":
    main()