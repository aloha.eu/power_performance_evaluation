from werkzeug.security import check_password_hash,generate_password_hash
from flask_mongoengine import MongoEngine
import datetime

db = MongoEngine()

class User(db.Document):

    email = db.StringField(max_length=120)
    #_password = db.StringField(max_length=256)
    #project = db.ListField(db.StringField(max_length=64))
    creation_date = db.DateTimeField()
    modified_date = db.DateTimeField(default=datetime.datetime.now)

    def __init__(self, *args, **kwargs):
        super(db.Document, self).__init__(*args, **kwargs)
        #self.login = db.StringField(max_length=80, unique=True)
        #self.email = db.StringField(max_length=120)
        self._password = db.StringField(max_length=256)
        #self.project = db.ListField(db.StringField(max_length=64))
        #self.creation_date = db.DateTimeField()
        #self.modified_date = db.DateTimeField(default=datetime.datetime.now)




    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)

    @property
    def password(self):
        print("getter of x password called")
        return self._password

    @password.setter
    def password(self, value):
        self._password = generate_password_hash(value)

    @staticmethod
    def validate_login(password_hash, password):
        return check_password_hash(password_hash, password)


#Dataset
class Dataset(db.Document):
    creator = db.StringField()
    creation_date = db.DateTimeField()
    path = db.StringField()
    samples_id = db.ListField()
    #number_labels = db.IntField()
    labels = db.ListField()
    data_resolution = db.ListField() #Should be a matrix
    encryption_type = db.StringField() #TO be checked
    label_x = db.ListField() # Array
    label_y = db.ListField() #Array
    training_flag = db.ListField()

    def get_id(self):
        return str(self.id)

    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)



class Project(db.Document):

    creator = db.StringField()
    dataset = db.StringField()
    creation_date = db.DateTimeField(default=datetime.datetime.now)
    modified_date = db.DateTimeField(default=datetime.datetime.now)

    def get_id(self):
        return str(self.id)

    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)


# Contrains use case
class ProjectContrains(db.Document):
    #name = db.StringField(max_length=64) #Restricted set of values. Power, Security_level, Execution time, Accuracy
    power_value = db.IntField()
    power_priority = db.IntField()

    security_value = db.IntField()
    security_priority = db.IntField()

    execution_time_value = db.IntField()
    execution_time_priority = db.IntField()

    accuracy_value = db.IntField()
    accuracy_priority = db.IntField()


    project = db.StringField()
    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)

#Conf file
class ConfigurationFile(db.Document):
    creation_date = db.DateTimeField()
    project = db.StringField()
    path = db.StringField()

    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)


class AlgorithmConfiguration(db.Document):
    creation_date = db.DateTimeField()
    path = db.StringField()
    project = db.StringField()

    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)


class Architecture(db.Document): #File
    creation_date = db.DateTimeField()
    path = db.StringField()
    project = db.StringField()

    def get_id(self):
        return str(self.id)


    def __str__(self):
        sb = []
        for key in self.__dict__:
            sb.append("{key}='{value}'".format(key=key, value=self.__dict__[key]))

        return ', '.join(sb)
