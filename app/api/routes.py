# -*- coding: utf-8 -*-

###
### DO NOT CHANGE THIS FILE
### 
### The code is auto generated, your change will be overwritten by 
### code generating.
###
from __future__ import absolute_import

from .api.eval_perf import EvalPerf


routes = [
    dict(resource=EvalPerf, urls=['/eval_perf'], endpoint='eval_perf'),
]