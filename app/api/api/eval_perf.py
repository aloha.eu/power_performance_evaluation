# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import request, g

from . import Resource
from .. import schemas
import os
import subprocess
from flask_mongoengine import MongoEngine
import subprocess
import os
from models.models import *
from perf_eval_call import *
from app.dbTst import *

class EvalPerf(Resource):

    def get(self):
        # Java perf_eval module call
        try:
            #print(g.args)
            dnn_id = g.args('algorithm_configuration_id')
            arch_id = g.args('architecture_id')
            dnn_path = models.AlgorithmConfiguration.objects.get(id=dnn_id).path
            arch_path = models.Architecture.objects.get(id=arch_id).path

            if dnn_path is None:
                return {"message": "DNN model not found", "code": 404}, 404

            if arch_path is None:
                return {"message": "Architecture not found", "code": 404}, 404

            cmd = ['java', '-jar', 'protobuf_interface.jar', '--evaluate', dnn_path]
            DEVNULL = os.open(os.devnull, os.O_WRONLY)
            process = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True, stderr=DEVNULL)
            timer = Timer(36000, process.kill)
            try:
                timer.start()
                stdout, stderr = process.communicate()
            finally:
                timer.cancel()

            result = stdout
            return result, 200

        except Exception:
            return {"message": "Internal error", "code": 406}, 406

        # TODO: ORM: pack result into a specified object to be stored in mongoDB: eval_result ??